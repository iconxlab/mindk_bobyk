<?php

namespace Controller;

use Libs\Controller;

/**
 * Controller for main page and general actions
 *
 * Class Index
 * @package Controller
 */
class Index extends Controller
{
	/**
	 * Main page action
	 */
	public function actionIndex()
	{
		$this->render('index/index');
	}

	/**
	 * Page about
	 */
	public function actionAbout()
	{
		$this->render('index/about');
	}

	/**
	 * Page contact
	 */
	public function actionContact()
	{
		$this->render('index/contact');
	}

	/**
	 * Not found page with 404 header
	 */
	public function actionNotfound()
	{
		$code = $this->_oView->getVar('errorCode');

		if(!$code)
			$code = 404;

		header("HTTP/1.0 ". $code);

		return $this->render('404');
	}

}