<div id="add-student">
	<form class="form-horizontal" action="" method="post" role="form" enctype="multipart/form-data">

		<?php if(!empty($row['id']) && $row['id'] > 0): ?>
			<input class="form-control" type="hidden" name="id" value="<?php echo $row['id']; ?>" />
		<?php endif; ?>

		<div class="form-group">
			<label class="col-sm-2 control-label">Name:</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="name" value="<?php echo !empty($row['name']) ? $row['name'] : ''; ?>" />
				<span class="has-error"><?php echo !empty($errors['name']) ? $errors['name'] : ''; ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Last name:</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="last_name" value="<?php echo !empty($row['last_name']) ? $row['last_name'] : ''; ?>" />
				<span class="has-error"><?php echo !empty($errors['last_name']) ? $errors['last_name'] : ''; ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Sex:</label>
			<div class="col-sm-4">
				<select name="sex" class="form-control">
					<?php foreach($aSex as $sexKey => $sexTitle): ?>
						<option <?php echo (!empty($row['sex']) && $sexKey == $row['sex']) ? ' selected=""' : ''; ?> value="<?php echo $sexKey; ?>"><?php echo $sexTitle; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Group:</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="group" value="<?php echo !empty($row['group']) ? $row['group'] : ''; ?>" />
				<span class="has-error"><?php echo !empty($errors['group']) ? $errors['group'] : ''; ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Faculty:</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="faculty" value="<?php echo !empty($row['faculty']) ? $row['faculty'] : ''; ?>" />
				<span class="has-error"><?php echo !empty($errors['faculty']) ? $errors['faculty'] : ''; ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Phone:</label>
			<div class="col-sm-4">
				<input class="form-control" type="tel" name="phone" value="<?php echo !empty($row['phone']) ? $row['phone'] : ''; ?>" />
				<span class="has-error"><?php echo !empty($errors['phone']) ? $errors['phone'] : ''; ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">E-mail:</label>
			<div class="col-sm-4">
				<input class="form-control" type="email" name="email" value="<?php echo !empty($row['email']) ? $row['email'] : ''; ?>" />
				<span class="has-error"><?php echo !empty($errors['email']) ? $errors['email'] : ''; ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Photo:</label>
			<div class="col-sm-4">
				<?php if(!empty($row['photo'])): ?>
					<input type="hidden" name="photo" value="<?php echo !empty($row['photo']) ? $row['photo'] : ''; ?>" />
					<img alt="" src="<?php echo $row['photo']; ?>" /><br/><br/>
				<?php endif; ?>
				<input class="form-control" type="file" name="photo" accept=".png,.jpg,.jpeg,.gif" />
			</div>
		</div>


		<div class="form-group">
			<label class="col-sm-2 control-label">&nbsp;</label>
			<div class="col-sm-4">
				<button class="btn btn-default" type="submit">Save</button>
			</div>
		</div>
	</form>


</div>