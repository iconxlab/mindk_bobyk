<div class="list">
	<?php if(!empty($rowset)): ?>
		<?php foreach($rowset as $row): ?>
			<div class="row">
				<div class="col-md-8">
					<a href="/student/<?php echo $row['id']; ?>">
						<span><?php echo $row['name']; ?></span>
						<span><?php echo $row['last_name']; ?></span>
					</a>

					<span><?php echo $row['group']; ?></span>
					<span><?php echo $row['faculty']; ?></span>
				</div>

				<div class="col-md-4 text-right">
					<a class="btn btn-link" href="/edit/<?php echo $row['id']; ?>">Edit</a>
					<a class="btn btn-danger" href="/delete/<?php echo $row['id']; ?>">Delete</a>
				</div>
			</div>
		<?php endforeach; ?>
	<?php else: ?>
		<div class="no-results">No results by request</div>
	<?php endif; ?>

	<?php echo $oPager->render(); ?>
</div>
