<?php

namespace Libs;


class Pager
{
	protected $_page = 1;

	protected $_pageSize = 10;

	protected $_total = 0;

	private $_countPages = 0;

	public function __construct()
	{}

	public function setTotal($total)
	{
		$this->_total = $total;

		return $this;
	}

	public function setPageSize($size)
	{
		$this->_pageSize = $size;

		return $this;
	}

	public function setPage($page)
	{
		$this->_page = $page;

		return $this;
	}

	public function getPage()
	{
		return $this->_page;
	}

	public function getPageSize()
	{
		return $this->_pageSize;
	}

	public function getTotal()
	{
		return $this->_total;
	}

	public function getOffset()
	{
		return (($this->_page - 1) * $this->_pageSize);
	}

	protected function _calcPages()
	{
		$this->_countPages = ceil($this->_total / $this->_pageSize);
	}

	/**
	 * Build pager
	 *
	 * @return string
	 */
	public function render()
	{
		$this->_calcPages();

		if($this->_countPages <= 1)
			return '';

		$url = !empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '';

		$url = preg_replace('`&?page\=\d+`iU', '', $url);
		$url = ($url ? $url . '&' : '?') . 'page=';

		$html = '<ul class="pager">';

		for($p = 1; $p <= $this->_countPages; $p++)
		{
			$html .= '<li>';

			if($this->_page == $p)
				$html .= '<span>'. $p .'</span>';
			else
				$html .= '<a href="'. ($url . $p) .'">'. $p .'</a>';

			$html .= '</li>';
		}

		$html .= '</ul>';

		return $html;
	}

} 