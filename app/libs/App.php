<?php
/**
 * Created by PhpStorm.
 * User: Andriy
 * Date: 05.03.14
 * Time: 16:18
 */

use Libs\HttpException;
use Libs\Model;
use Libs\View;

class App
{
	/**
	 * Instance current class
	 *
	 * @var App
	 */
	private static $_instance;

	/**
	 * Return single instance
	 *
	 * @return App
	 */
	public static function getInstance()
	{
		if(self::$_instance === null)
			self::$_instance = new self;

		return self::$_instance;
	}

	/**
	 * Project configs
	 *
	 * @var array
	 */
	private $_config = array();

	/**
	 * Current controller
	 *
	 * @var string
	 */
	private $_controller = 'index';

	/**
	 * Current action
	 *
	 * @var string
	 */
	private $_action = 'notfound';

	/**
	 * MVC View
	 *
	 * @var \Libs\View
	 */
	private $_oView;

	/**
	 * Disable public constructor
	 */
	private function __conscruct() {}

	/**
	 * Disable cloning mode
	 */
    private function __clone() {}

	/**
	 * Disable creating object by unserialize
	 */
    private function __wakeup() {}



	/**
	 * Return current controller name
	 *
	 * @return string
	 */
	public function getController()
	{
		return $this->_controller;
	}

	/**
	 * Return current action name
	 *
	 * @return string
	 */
	public function getAction()
	{
		return $this->_action;
	}

	/**
	 * Set project configs
	 *
	 * @param array $config
	 * @return $this
	 */
	public function setConfig(array $config)
	{
		$this->_config = $config;

		return $this;
	}

	/**
	 * Return configs as object
	 *
	 * @return object
	 */
	public function getConfig()
	{
		return (object)$this->_config;
	}

	/**
	 * Run application
	 */
	public function run()
	{
		spl_autoload_register(array($this, 'autoload'));

		$this->_initDb();

		$viewClass = $this->getConfig()->viewClass;
		$this->_oView = new $viewClass;
		$this->_oView->setTemplatePath(APPLICATION_PATH . $this->getConfig()->templatePath)
			  ->setTemplate($this->getConfig()->defaultTemplate)
			  ->setViewPath(APPLICATION_PATH . $this->getConfig()->viewPath);

		if($this->prepareRoute() === false)
		{
			$this->_controller = 'Index';
			$this->_action = 'notfound';
		}

		try {
			$this->_runController($this->_controller, $this->_action, $this->_oView);
		}
		// If httexeption, show page by code
		catch(HttpException $e) {
			$this->_oView->setVar('errorMessage', $e->getMessage());
			$this->_oView->setVar('errorCode', $e->getCode());

			$this->forward('index', 'notfound');
		}
		// If another exception, show error 404
		catch(Exception $e) {
			$this->_oView->setVar('errorMessage', 'Page not found');
			$this->_oView->setVar('errorCode', 404);

			$this->forward('index', 'notfound');
		}
	}

	/**
	 * Call action in controller
	 *
	 * @param $controller
	 * @param $action
	 * @param View $oView
	 * @throws Exception
	 */
	public function _runController($controller, $action, View $oView = null)
	{
		$controller = ucfirst($controller);
		$controllerPath = APPLICATION_PATH . '/controllers/'. $controller .'.php';

		// If not exist controller, throw exception 404
		if(!file_exists($controllerPath))
			throw new Exception('Controller '. $controller .' not found.');

		require_once $controllerPath;

		$controller = "\Controller\\". $controller;
		$oController = new $controller;

		// Set View to controller
		$oController->setView($oView);

		$action = 'action'.ucfirst($action);

		// If not exist action, throw exception 404
		if(!method_exists($oController, $action))
			throw new Exception('Action '. $action .' not found.');

		call_user_func(array($oController, $action));

		if($oView)
			$oView->render();
	}

	/**
	 * Forward to other controller and action
	 *
	 * @param $controller
	 * @param $action
	 */
	public function forward($controller, $action)
	{
		$this->_runController($controller, $action, $this->_oView);

		// End script after rendering current action
		exit;
	}

	/**
	 * Initialize database for model
	 */
	protected function _initDb()
	{
		require_once APPLICATION_PATH . '/libs/dbsimple/config.php';
		require_once APPLICATION_PATH . '/libs/dbsimple/Connect.php';
		require_once APPLICATION_PATH . '/libs/Model.php';

		$dsn =	$this->_config['db']['driver'] .'://'.
			$this->_config['db']['user'] .':'. $this->_config['db']['password'] .'@'.
			$this->_config['db']['host'] .'/'. $this->_config['db']['db'] .'?charset='. $this->_config['db']['charset'];

		$db = new \DbSimple_Connect($dsn);

		Model::setDbDriver($db);
	}

	/**
	 * Set controller and action by request
	 *
	 * @return bool
	 */
	public function prepareRoute()
	{
		$url = $_SERVER['REQUEST_URI'];

		// Find route by request
		$router = $this->_findRouteByUrl($url, $this->_config['routes']);

		// If route is found
		if($router)
		{
			// Slicing route. First element is Controller, second is Action
			$aRouter = explode('/', $router);

			if(!empty($aRouter[0]) && !empty($aRouter[1]))
			{
				$this->_controller = $aRouter[0]; // Set controller
				$this->_action = $aRouter[1]; // Set action
			}

			return true;
		}

		return false;
	}

	/**
	 * Find route by request url
	 *
	 * @param $url
	 * @param $routes
	 * @return bool
	 */
	private function _findRouteByUrl($url, $routes)
	{
		// Cut get params
		$url = preg_replace('`\?.*$`i', '', $url);

		// Slicing request url
		$aUrl = explode('/', trim($url, '/'));

		// Combine clean url params for checking by regexp
		$regUrl = implode('/', $aUrl);

		// For each route
		foreach($routes as $route => $actionPath)
		{
			$sRoute = trim($route, '/');

			// Find vars in route url
			$matchedParams = array();
			preg_match_all('`<(\w+)\:(.*)>`iU', $route, $matchedParams);

			if(!empty($matchedParams[1]) && !empty($matchedParams[2]))
			{
				foreach($matchedParams[2] as $i => $key)
				{
					// Replace var with regexp on var
					$sRoute = str_replace('<'. $matchedParams[1][$i] .':'. $key .'>', '('.$key.')', $sRoute);
				}
			}

			// Check match route with request
			$reg = '`^'.trim($sRoute, '/').'$`i';
			if($sRoute == $regUrl || preg_match($reg, $regUrl, $matchedValues))
			{
				if(!empty($matchedValues))
				{
					foreach($matchedValues as $i => $val)
					{
						if(!empty($matchedParams[1][$i-1]))
						{
							// Set params from request by route url
							$_GET[$matchedParams[1][$i-1]] = $val;
							$_REQUEST[$matchedParams[1][$i-1]] = $val;
						}
					}
				}

				// if success matched, return route
				return $actionPath;
			}
		}

		// Checking may url is controller/action
		if(count($aUrl) >= 1)
		{
			// Controller first element, action second
			if(count($aUrl) > 1)
			{
				return $aUrl[0] . '/' . $aUrl[1];
			}

			// Default controller, action from url
			return 'index/' . $aUrl[0];
		}

		return false;
	}

	/**
	 * Autoload class
	 *
	 * @param $class
	 */
	public function autoload($class)
	{
		$aPath = explode('\\', $class);

		$requirePath = '';
		if(!empty($aPath[0]))
			$requirePath .= lcfirst($aPath[0]);

		if(!empty($aPath[1]))
			$requirePath .= '/'. $aPath[1].'.php';

		$requirePath = APPLICATION_PATH .'/'. $requirePath;

		if(is_file($requirePath))
		{
			require_once $requirePath;
		}
	}

	/**
	 * Query logger for dbsimple
	 *
	 * @param $db
	 * @param $sql
	 * @param $args
	 */
	public static function queryLogger($db, $sql, $args)
	{
//		echo $sql;
//		print_r($args);
//		print("Query::".$query);
	}

} 