<?php

namespace Libs;

/**
 * Validation exception
 *
 * Class ValidationException
 * @package Libs
 */
class ValidationException extends \Exception
{
	protected $_messages = array();

	public function __construct($messages)
	{
		if(is_array($messages))
		{
			$this->_messages = array_merge($this->_messages, $messages);
		}
		else
		{
			$this->_messages[] = $messages;
		}
	}

	public function getMessages()
	{
		return $this->_messages;
	}
} 