<?php

namespace Libs;

use \Libs\TwigAsset;

/**
 * MVC View by Twig
 *
 * Class TwigView
 * @package Libs
 */
class TwigView extends View
{
	/**
	 * Get name template for render
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function getTemplate()
	{
		return $this->_template . '.twig';
	}

	/**
	 * Render view by name and return data
	 *
	 * @param $part
	 * @return string
	 * @throws \Exception
	 */
	public function getView($part)
	{
		if(empty($this->_views[$part]))
			return '';

		$twig = new \Twig_Environment(
			new \Twig_Loader_Filesystem($this->_viewPath)
		);

		$html = $twig->render($this->_views[$part] . '.twig', $this->_vars);

		return $html;
	}

	/**
	 * Render template
	 * * @throws \Exception
	 */
	public function render()
	{
		$this->setVar('layout', $this);

		$twig = new \Twig_Environment(
			new \Twig_Loader_Filesystem($this->_templatePath)
		);

		echo $twig->render($this->getTemplate(), $this->_vars);
	}

} 