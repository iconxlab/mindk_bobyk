<?php
/**
 * Created by PhpStorm.
 * User: quix
 * Date: 16.12.14
 * Time: 12:03
 */

namespace Libs;


class View
{
	/**
	 * Path to templates
	 *
	 * @var string
	 */
	protected $_templatePath = '';

	/**
	 * Path to views
	 *
	 * @var string
	 */
	protected $_viewPath = '';

	/**
	 * Template name for render
	 *
	 * @var string
	 */
	protected $_template = '';

	/**
	 * Views for render
	 *
	 * @var array
	 */
	protected $_views = array();

	/**
	 * Variables for view
	 *
	 * @var array
	 */
	protected $_vars = array();

	/**
	 * Set variable for view
	 *
	 * @param $key
	 * @param $val
	 *
	 * @return $this - For chain
	 */
	public function setVar($key, $val)
	{
		$this->_vars[$key] = $val;

		return $this;
	}

	/**
	 * Get view variable
	 *
	 * @param $key
	 * @return null - if variable not set
	 */
	public function getVar($key)
	{
		if (isset($this->_vars[$key]))
			return $this->_vars[$key];

		return null;
	}

	/**
	 * Get value from view vars
	 *
	 * @param $key
	 * @return null
	 */
	public function __get($key)
	{
		if(!empty($this->_vars[$key]))
			return $this->_vars[$key];

		return null;
	}

	/**
	 * Set value to view vars
	 *
	 * @param $key
	 * @param $value
	 */
	public function __set($key, $value)
	{
		$this->_vars[$key] = $value;
	}

	/**
	 * Check exist view variable
	 *
	 * @param $key
	 * @return bool
	 */
	public function __isset($key)
	{
		return isset($this->_vars[$key]);
	}

	/**
	 * Check is not empty view variable
	 *
	 * @param $key
	 * @return bool
	 */
	public function __empty($key)
	{
		return empty($this->_vars[$key]);
	}

	/**
	 * Set templates path
	 *
	 * @param $path
	 */
	public function setTemplatePath($path)
	{
		$this->_templatePath = $path;

		return $this;
	}

	/**
	 * Set path to views
	 *
	 * @param $path
	 */
	public function setViewPath($path)
	{
		$this->_viewPath = $path;

		return $this;
	}

	/**
	 * Set template name for render
	 *
	 * @param $template
	 * @return $this
	 */
	public function setTemplate($template)
	{
		$this->_template = $template;

		return $this;
	}

	/**
	 * Get path to template for render
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function getTemplate()
	{
		$template = rtrim($this->_templatePath, '/') . '/'
						. $this->_template . '.php';

		if(!file_exists($template))
			throw new \Exception('Template '. $template .' id not found.');

		return $template;
	}

	/**
	 * Set view name for render.
	 * View invoked	from template by method getView($viewName).
	 *
	 * @param $view - view name
	 * @param null $part - template part
	 * @return $this - for chain
	 */
	public function setView($view, $part = null)
	{
		if($part === null)
			$part = 'content';

		$this->_views[$part] = $view;

		return $this;
	}

	/**
	 * Render view by name and return data
	 *
	 * @param $part
	 * @return string
	 * @throws \Exception
	 */
	public function getView($part)
	{
		if(empty($this->_views[$part]))
			return '';

		$view = rtrim($this->_viewPath, '/'). '/'. $this->_views[$part] . '.php';

		if(!file_exists($view))
			throw new \Exception('File '. $view . ' is not found.');

		extract($this->_vars);

		ob_start();

		include $view;

		$html = ob_get_clean();

		return $html;
	}

	/**
	 * Render template
	 * * @throws \Exception
	 */
	public function render()
	{
		ob_start();

		include $this->getTemplate();

		$html = ob_get_clean();

		echo $html;
	}

} 