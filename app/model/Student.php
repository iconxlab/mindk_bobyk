<?php
/**
 * Created by PhpStorm.
 * User: Andriy
 * Date: 06.03.14
 * Time: 9:40
 */

namespace Model;

use Libs\Model;

/**
 * Model for students table
 *
 * Class Student
 * @package Model
 */
class Student extends Model
{
	/**
	 * Fields in table
	 *
	 * @var array
	 */
	protected $_allowFields = array(
		'id',
		'name',
		'last_name',
		'sex',
		'group',
		'faculty',
		'phone',
		'email',
		'photo',
	);

	/**
	 * Validation for fields
	 *
	 * @var array
	 */
	protected $_fieldsValidation = array(
		'name' => array(
			'regexp' => '[\w\s\-\_]{2,45}',
			'message' => 'Allow a-z A-Z. Min length 2 char, max 45.',
		),
		'last_name' => array(
			'regexp' => '[\w\s\-\_]{2,45}',
			'message' => 'Allow a-z A-Z. Min length 2 char, max 45.',
		),
		'group' => array(
			'regexp' => '[\w\s\-\_]{2,45}',
			'message' => 'Allow a-z A-Z. Min length 2 char, max 45.',
		),
		'faculty' => array(
			'regexp' => '[\w\s\-\_]{2,45}',
			'message' => 'Allow a-z A-Z. Min length 2 char, max 45.',
		),
		'phone' => array(
			'regexp' => '\+?\d{8,12}',
			'message' => 'Please, type your full phone number.',
		),
		'email' => array(
			'filter' => FILTER_VALIDATE_EMAIL,
			'message' => 'Please, type correct email address.',
		),
	);

	const SEX_MEN = 'men';
	const SEX_WOMEN = 'women';

	/**
	 * All available sex
	 *
	 * @var array
	 */
	protected static $_aSex = array(
		self::SEX_MEN => 'Men',
		self::SEX_WOMEN => 'Women',
	);

	/**
	 * Return all available sex
	 *
	 * @param null $key
	 * @return array
	 */
	public static function getSex($key = null)
	{
		if($key !== null && isset(self::$_aSex[$key]))
			return self::$_aSex[$key];

		return self::$_aSex;
	}

	/**
	 * Get list by pager
	 *
	 * @param \Libs\Pager $oPager
	 * @return mixed
	 */
    public function getList(\Libs\Pager $oPager = null)
    {
        $rowset = $this->_db->select('
            SELECT id, `name`, last_name, `group`, faculty, phone, email
		  		FROM student
					ORDER BY id ASC
		  		{LIMIT ?d, ?d}
            ',
            ($oPager ? $oPager->getOffset() : DBSIMPLE_SKIP),
            ($oPager ? $oPager->getPageSize() : DBSIMPLE_SKIP)
        );

        // Set total count to pager
        if($oPager)
        {
            $oPager->setTotal(
                $this->_db->selectCell(
                    'SELECT count(id) FROM student'
                )
            );
        }

        return $rowset;
    }

	/**
	 * Return student by ID
	 *
	 * @param $id
	 * @return array|mixed
	 */
    public function getRow($id)
    {
        if($id <= 0)
            return array();

        $row = $this->_db->selectRow('
			SELECT * FROM student
			WHERE id=?d',
            $id
        );

        return $row;
    }

	/**
	 * Save student data
	 * If isset id update data, otherwise create new item
	 *
	 * @param $data
	 * @return mixed|null
	 */
	public function save($data)
	{
		if(empty($data))
			return null;

		$id = !empty($data['id']) ? $data['id'] : null;

		// Select data by id
		$row = $this->getRow($id);

		// Merge old and new data
		$row = array_merge($row, $data);

		// Delete useless keys in $row
		$row = $this->prepareRow($row);

		$this->validateRow($row);

		if(!empty($row['id']) && $id > 0)
		{
			unset($row['id']);

			$this->_db->query("UPDATE student SET ?a WHERE id = ?d", $row, $id);
		}
		else
		{
			$id = $this->_db->query(
				"INSERT INTO student (?#) VALUES(?a)",
				array_keys($row),
				array_values($row)
			);
		}

		return $id;
	}

	/**
	 * Delete student by id
	 *
	 * @param $id
	 * @return bool|mixed
	 */
	public function deleteById($id)
	{
		if($id <= 0)
			return false;

		return $this->_db->query('DELETE FROM student WHERE id = ?d', $id);
	}

}